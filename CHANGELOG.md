# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.1.0](https://gitlab.com/handmade-devops/data-service/compare/v1.0.1...v1.1.0) (2021-03-12)


### Features

* :star2: added Docker ([b7b3921](https://gitlab.com/handmade-devops/data-service/commit/b7b392165b7782ecfc126fb56667c8141fcb70e7))
* added Express app ([0ed08c0](https://gitlab.com/handmade-devops/data-service/commit/0ed08c0671ebe9de2a5d270da6d65343347fdcff))

### 1.0.1 (2021-03-11)


### Bug Fixes

* :pencil: fixed package name ([70bff92](https://gitlab.com/handmade-devops/data-service/commit/70bff92ddbde97cbc5b1cad03786c32f328b728b))

## 1.0.0 (2021-03-11)
