CREATE TABLE IF NOT EXISTS organisation (
  id serial PRIMARY KEY,
  name VARCHAR NOT NULL,
  description VARCHAR,
  logo_path VARCHAR,
  unique (name)
);

CREATE TYPE roles AS ENUM ('BUYER', 'SELLER');
CREATE TABLE IF NOT EXISTS client (
  id serial PRIMARY KEY,
  username VARCHAR NOT NULL,
  password VARCHAR NOT NULL,
  email VARCHAR NOT NULL,
  organisation_id INTEGER REFERENCES organisation(id),
  user_role roles NOT NULL,
  refresh INTEGER default 0,
  unique (username)
);

CREATE TABLE IF NOT EXISTS campaign (
  id serial PRIMARY KEY,
  name VARCHAR,
  organisation_id INTEGER NOT NULL REFERENCES organisation(id) ON DELETE CASCADE,
  description VARCHAR,
  cover_path VARCHAR,
  start_date timestamp default current_timestamp,
  end_date timestamp,
  unique (name, organisation_id)
);

CREATE TABLE IF NOT EXISTS product (
  id serial PRIMARY KEY,
  campaign_id INTEGER NOT NULL REFERENCES campaign(id) ON DELETE CASCADE,
  name VARCHAR,
  description VARCHAR,
  image_path VARCHAR,
  price NUMERIC(10, 2) default 0.0,
  total INTEGER NOT NULL default 1,
  sold INTEGER default 0,
  unique (name, campaign_id)
);

CREATE TABLE IF NOT EXISTS basket (
  id serial PRIMARY KEY,
  user_id INTEGER NOT NULL REFERENCES client(id) ON DELETE CASCADE,
  product_id INTEGER NOT NULL REFERENCES product(id) ON DELETE CASCADE,
  count INTEGER,
  purchased BOOL default false
);
