const { query } = require('../../utils/data')
const { ServerError } = require('../../utils/ServerError')
const logger = require('../../utils/logger')
const { formatQueryString } = require('../../utils/helpers')

const getAll = async (offset, resultsPerPage) => {
    logger.info('Getting all products...')

    return await query('SELECT * FROM product order by id desc offset $1 rows fetch next $2 rows only', [
        offset,
        resultsPerPage,
    ])
}

const getForCampaign = async (campaign_id) => {
    logger.info('Getting all campaigns...')

    const products = await query('SELECT * FROM product where campaign_id=$1', [campaign_id])

    return products
}

const create = async ({ name, campaign_id, description, price, total }) => {
    logger.info(`Adding product with name ${name}`)

    if (total <= 0 || !Number.isInteger(total)) {
        throw new ServerError(`'total' must be a positive integer number.`, 400)
    }

    try {
        const products = await query(
            'INSERT INTO product (name, campaign_id, description, price, total) VALUES ($1, $2, $3, $4, $5) RETURNING *',
            [name, campaign_id, description, price, total],
        )
        return products[0]

    } catch (e) {
        if (e.code === '23503') {
            throw new ServerError(`Product does not exist ${e.detail}`, 404)
        } else if (e.code === '23505') {
            throw new ServerError(`Unique error ${e.detail}`, 404)
        }
        throw e
    }
}

const getById = async (id) => {
    logger.info(`Getting product with id ${id}`)

    const products = await query('SELECT * FROM product WHERE id=$1', [id])

    if (products.length !== 1) {
        throw new ServerError('Product does not exist!', 404)
    }

    return products[0]
}

const update = async (id, params) => {
    logger.info(`Update product with id ${id}`)

    if (params.total <= 0 || !Number.isInteger(params.total)) {
        throw new ServerError(`'total' must be a positive integer number.`, 400)
    }

    const [queryFormat, queryArray] = formatQueryString(params)
    try {
        const products = await query(
            `UPDATE product SET ${queryFormat} WHERE id=$1 RETURNING *`,
            [id, ...queryArray],
        )

        if (products.length !== 1) {
            throw new ServerError('Product does not exist', 404)
        }

        return products[0]
    } catch (e) {
        if (e.code === '23503') {
            throw new ServerError(`Product does not exist ${e.detail}`, 404)
        } else if (e.code === '23505') {
            throw new ServerError(`Unique error ${e.detail}`, 404)
        }
        throw e
    }
}

const sell = async (id, { count }) => {
    logger.info(`Selling product with id ${id}`)

    const products = await query('SELECT * FROM product WHERE id=$1', [id])

    if (products.length !== 1) {
        throw new ServerError('Product does not exist!', 404)
    }

    const { total, sold } = products[0]

    if (total - sold < count) {
        throw new ServerError('Not enough units left', 400)
    }

    const updatedProducts = await query(`Update product SET sold=$2 WHERE id=$1 RETURNING *`, [id, sold + count])

    return updatedProducts[0]
}

const remove = async (id) => {
    logger.info(`Deleting product with id ${id}`)

    await query('DELETE FROM product WHERE id=$1', [id])
}

module.exports = {
    getAll,
    getForCampaign,
    create,
    getById,
    update,
    sell,
    remove
}

