const { getAll, getForCampaign, create, getById, update, sell, remove } = require('./service')
const Router = require('express').Router

const router = Router()

router.get('/', async (req, res) => {
    let { page, limit } = req.query
    if (!page) page = 0
    if (!limit) limit = 10

    const offset = page * limit;
    const products = await getAll(offset, limit)

    res.json(products)
})

router.get('/campaign/:id', async (req, res) => {
    const { id } = req.params
    const products = await getForCampaign(id)

    res.json(products)
})

router.post('/', async (req, res) => {
    const { name, campaign_id, description, price, total } = req.body
    const product = await create({ name, campaign_id, description, price, total })

    res.status(201).json(product)
})

router.get('/:id', async (req, res) => {
    const { id } = req.params

    const product = await getById(id)

    res.json(product)
})

router.put('/:id', async (req, res) => {
    const { id } = req.params
    const { name, campaign_id, description, price, total } = req.body

    const product = await update(id, { name, campaign_id, description, price, total })

    res.json(product)
})

router.put('/sell/:id', async (req, res) => {
    const { id } = req.params
    const { count } = req.body

    const product = await sell(id, { count })

    res.json(product)
})

router.delete('/:id', async (req, res) => {
    const { id } = req.params

    await getById(id)

    await remove(id)

    res.status(204).end()
})

module.exports = router
