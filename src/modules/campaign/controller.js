const { getAll, getForOrganisation, create, getById, update, remove } = require('./service')
const Router = require('express').Router

const router = Router()

router.get('/', async (req, res) => {
    let { page, limit } = req.query
    if (!page) page = 0
    if (!limit) limit = 10

    const offset = page * limit;
    const campaigns = await getAll(offset, limit)

    res.json(campaigns)
})

router.get('/organisation/:id', async (req, res) => {
    const { id } = req.params
    const campaigns = await getForOrganisation(id)

    res.json(campaigns)
})

router.post('/', async (req, res) => {
    const { name, organisation_id, description, start_date, end_date } = req.body
    const campaign = await create({ name, organisation_id, description, start_date, end_date })

    res.status(201).json(campaign)
})

router.get('/:id', async (req, res) => {
    const { id } = req.params

    const campaign = await getById(id)

    res.json(campaign)
})

router.put('/:id', async (req, res) => {
    const { id } = req.params
    const { name, organisation_id, description, start_date, end_date, cover_path } = req.body

    const campaign = await update(id, { name, organisation_id, description, start_date, end_date, cover_path })

    res.json(campaign)
})

router.delete('/:id', async (req, res) => {
    const { id } = req.params

    await getById(id)

    await remove(id)

    res.status(204).end()
})

module.exports = router
