const { query } = require('../../utils/data')
const { ServerError } = require('../../utils/ServerError')
const logger = require('../../utils/logger')
const { formatQueryString } = require('../../utils/helpers')

const getAll = async (offset, resultsPerPage) => {
    logger.info('Getting all campaigns...')

    return await query('SELECT * FROM campaign order by id desc offset $1 rows fetch next $2 rows only', [
        offset,
        resultsPerPage,
    ])
}

const getForOrganisation = async (organisation_id) => {
    logger.info(`Getting all campaigns for organisation ${organisation_id}`)

    const campaigns = await query('SELECT * FROM campaign WHERE organisation_id=$1', [organisation_id])

    return campaigns
}

const create = async ({ name, organisation_id, description, start_date, end_date }) => {
    logger.info(`Adding campaign with name ${name}`)

    if (new Date(start_date) >= new Date(end_date)) {
        throw new ServerError(`Start date cannot be after end date.`, 400)
    }

    try {
        const campaigns = await query(
            'INSERT INTO campaign (name, organisation_id, description, start_date, end_date) VALUES ($1, $2, $3, $4, $5) RETURNING *',
            [name, organisation_id, description, start_date, end_date],
        )
        return campaigns[0]

    } catch (e) {
        if (e.code === '23503') {
            throw new ServerError(`Organisation does not exist ${e.detail}`, 404)
        } else if (e.code === '23505') {
            throw new ServerError(`Campaign name already exists`, 409)
        }
        throw e
    }
}

const getById = async (id) => {
    logger.info(`Getting campaign with id ${id}`)

    const campaigns = await query('SELECT * FROM campaign WHERE id=$1', [id])

    if (campaigns.length !== 1) {
        throw new ServerError('Campaign does not exist!', 404)
    }

    return campaigns[0]
}

const update = async (id, params) => {
    logger.info(`Update campaign with id ${id}`)

    const campaign = await getById(id);
    const start_date = params.start_date ?? campaign.start_date
    const end_date = params.end_date ?? campaign.end_date

    if (new Date(start_date) >= new Date(end_date)) {
        throw new ServerError(`Start date cannot be after end date.`, 400)
    }

    const [queryFormat, queryArray] = formatQueryString(params)
    try {
        const campaigns = await query(
            `UPDATE campaign SET ${queryFormat} WHERE id=$1 RETURNING *`,
            [id, ...queryArray],
        )

        if (campaigns.length !== 1) {
            throw new ServerError('Campaign does not exist', 404)
        }

        return campaigns[0]
    } catch (e) {
        if (e.code === '23503') {
            throw new ServerError(`Organisation does not exist ${e.detail}`, 404)
        } else if (e.code === '23505') {
            throw new ServerError(`Campaign name already exists`, 409)
        }
        throw e
    }
}

const remove = async (id) => {
    logger.info(`Deleting campaign with id ${id}`)

    await query('DELETE FROM campaign WHERE id=$1', [id])
}

module.exports = {
    getAll,
    getForOrganisation,
    create,
    getById,
    update,
    remove
}

