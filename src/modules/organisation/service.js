const { query } = require('../../utils/data')
const { ServerError } = require('../../utils/ServerError')
const logger = require('../../utils/logger')
const { formatQueryString } = require('../../utils/helpers')

const getAll = async (offset, resultsPerPage) => {
    logger.info('Getting all organisations...')

    return await query('SELECT * FROM organisation order by id desc offset $1 rows fetch next $2 rows only', [
        offset,
        resultsPerPage,
    ])
}

const create = async ({ name, description }) => {
    logger.info(`Adding organisation with name ${name}`)

    try {
        const organisations = await query(
            'INSERT INTO organisation (name, description) VALUES ($1, $2) RETURNING *',
            [name, description],
        )
        return organisations[0]

    } catch (e) {
        if (e.code === '23505') {
            throw new ServerError(`Organisation name already exists`, 409)
        }
        throw e
    }
}

const getById = async ({ id }) => {
    logger.info(`Getting organisation with id ${id}`)

    const organisations = await query('SELECT * FROM organisation WHERE id=$1', [id])

    if (organisations.length !== 1) {
        throw new ServerError('Organisation does not exist!', 404)
    }

    return organisations[0]
}

const update = async (id, params) => {
    logger.info(`Update organisation with id ${id}`)

    const [queryFormat, queryArray] = formatQueryString(params)
    try {
        const organisations = await query(
            `UPDATE organisation SET ${queryFormat} WHERE id=$1 RETURNING *`,
            [id, ...queryArray],
        )

        if (organisations.length !== 1) {
            throw new ServerError('Organisation does not exist', 404)
        }

        return organisations[0]
    } catch (e) {
        if (e.code === '23505') {
            throw new ServerError('Organisation name already exists!', 409)
        }
        throw e
    }
}

const remove = async ({ id }) => {
    logger.info(`Deleting organisation with id ${id}`)

    await query('DELETE FROM organisation WHERE id=$1', [id])
}

module.exports = {
    getAll,
    create,
    getById,
    update,
    remove
}

