const { getAll, create, getById, update, remove } = require('./service')
const Router = require('express').Router

const router = Router()

router.get('/', async (req, res) => {
    let { page, limit } = req.query
    if (!page) page = 0
    if (!limit) limit = 10

    const offset = page * limit;
    const organisations = await getAll(offset, limit)

    res.json(organisations)
})

router.post('/', async (req, res) => {
    const { name, description } = req.body
    const organisation = await create({ name, description })

    res.status(201).json(organisation)
})

router.get('/:id', async (req, res) => {
    const { id } = req.params

    const organisation = await getById({ id })

    res.json(organisation)
})

router.put('/:id', async (req, res) => {
    const { id } = req.params
    const { name, description, logo_path } = req.body

    const organisation = await update(id, { name, description, logo_path })

    res.json(organisation)
})

router.delete('/:id', async (req, res) => {
    const { id } = req.params

    await getById({ id })

    await remove({ id })

    res.status(204).end()
})

module.exports = router
