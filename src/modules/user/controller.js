const { getAll, create, getById, getByUsername, update, remove } = require('./service')
const Router = require('express').Router

const router = Router()

router.get('/', async (req, res) => {
    const users = await getAll()

    res.json(users)
})

router.post('/', async (req, res) => {
    const { username, password, email, user_role } = req.body
    const user = await create({ username, password, email, user_role })

    res.status(201).json(user)
})

router.get('/:id', async (req, res) => {
    const { id } = req.params

    const user = await getById(id)

    res.json(user)
})

router.get('/username/:username', async (req, res) => {
    const { username } = req.params

    const user = await getByUsername(username)

    res.json(user)
})

router.put('/:id', async (req, res) => {
    const { id } = req.params
    const { username, password, email, organisation_id, user_role, refresh } = req.body

    const user = await update(id, { username, password, email, organisation_id, user_role, refresh })

    res.json(user)
})

router.delete('/:id', async (req, res) => {
    const { id } = req.params

    await getById(id)

    await remove(id)

    res.status(204).end()
})

module.exports = router
