const { query } = require('../../utils/data')
const { ServerError } = require('../../utils/ServerError')
const logger = require('../../utils/logger')
const { formatQueryString } = require('../../utils/helpers')

const getAll = async () => {
    logger.info('Getting all users...')

    return await query('SELECT * FROM client')
}

const create = async ({ username, password, email, user_role }) => {
    logger.info(`Adding user ${username}`)

    try {
        const users = await query(
            'INSERT INTO client (username, password, email, user_role) VALUES ($1, $2, $3, $4) RETURNING *',
            [username, password, email, user_role],
        )
        return users[0]

    } catch (e) {
        if (e.code === '23503') {
            throw new ServerError(`Organisation does not exist ${e.detail}`, 404)
        } else if (e.code === '23505') {
            throw new ServerError(`Username already exists`, 400)
        } else if (e.code === '22P02') {
            throw new ServerError(`Invalid value for user_role: ${e.message}`, 400)
        }
        throw e
    }
}

const getById = async (id) => {
    logger.info(`Getting user with id ${id}`)

    const users = await query('SELECT * FROM client WHERE id=$1', [id])

    if (users.length !== 1) {
        throw new ServerError('User does not exist!', 404)
    }

    return users[0]
}

const getByUsername = async (username) => {
    logger.info(`Getting user with username ${username}`)

    const users = await query('SELECT * FROM client WHERE username=$1', [username])

    if (users.length !== 1) {
        throw new ServerError('User does not exist!', 404)
    }

    return users[0]
}

const update = async (id, params) => {
    logger.info(`Update user with id ${id}`)

    const [queryFormat, queryArray] = formatQueryString(params)
    try {
        const users = await query(
            `UPDATE client SET ${queryFormat} WHERE id=$1 RETURNING *`,
            [id, ...queryArray],
        )

        if (users.length !== 1) {
            throw new ServerError('User does not exist', 404)
        }

        return users[0]
    } catch (e) {
        if (e.code === '23503') {
            throw new ServerError(`Organisation does not exist ${e.detail}`, 404)
        } else if (e.code === '23505') {
            throw new ServerError(`User name already exists`, 400)
        } else if (e.code === '22P02') {
            throw new ServerError(`Invalid value for user_role: ${e.message}`, 400)
        }
        throw e
    }
}

const remove = async (id) => {
    logger.info(`Deleting user with id ${id}`)

    await query('DELETE FROM client WHERE id=$1', [id])
}

module.exports = {
    getAll,
    create,
    getById,
    getByUsername,
    update,
    remove
}

