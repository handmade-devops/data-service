const { getAll, getForUser, create, getById, update, remove } = require('./service')
const Router = require('express').Router

const router = Router()

router.get('/', async (req, res) => {
    const baskets = await getAll()

    res.json(baskets)
})

router.get('/user/:userId', async (req, res) => {
    const { userId } = req.params
    const baskets = await getForUser(userId)

    res.json(baskets)
})

router.post('/', async (req, res) => {
    const { user_id, product_id, count } = req.body
    const basket = await create({ user_id, product_id, count })

    res.status(201).json(basket)
})

router.get('/:id', async (req, res) => {
    const { id } = req.params

    const basket = await getById(id)

    res.json(basket)
})

router.put('/:id', async (req, res) => {
    const { id } = req.params
    const { user_id, product_id, count, purchased } = req.body

    const basket = await update(id, { user_id, product_id, count, purchased })

    res.json(basket)
})

router.delete('/:id', async (req, res) => {
    const { id } = req.params

    await getById(id)

    await remove(id)

    res.status(204).end()
})

module.exports = router
