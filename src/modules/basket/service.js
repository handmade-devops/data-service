const { query } = require('../../utils/data')
const { ServerError } = require('../../utils/ServerError')
const logger = require('../../utils/logger')

const getAll = async () => {
    logger.info('Getting all baskets...')

    return await query('SELECT * FROM basket')
}

const getForUser = async (user_id) => {
    logger.info(`Getting all baskets for user ${user_id}`)

    const baskets = await query('SELECT * FROM basket WHERE user_id=$1', [user_id])

    return baskets
}

const create = async ({ user_id, product_id, count }) => {
    logger.info(`Adding basket for user ${user_id}`)

    if (count <= 0 || !Number.isInteger(count)) {
        throw new ServerError(`'count' must be a positive integer number.`, 400)
    }

    // validate the count for the product
    const products = await query('SELECT * FROM product WHERE id=$1', [product_id])

    if (products.length !== 1) {
        throw new ServerError('Product does not exist!', 404)
    }

    if (products[0].total - products[0].sold < count) {
        throw new ServerError(`'count' exceeds the available value`, 400)
    }

    try {
        const baskets = await query(
            'INSERT INTO basket (user_id, product_id, count) VALUES ($1, $2, $3) RETURNING *',
            [user_id, product_id, count],
        )
        return baskets[0]

    } catch (e) {
        if (e.code === '23503') {
            throw new ServerError(`Element with foreign key does not exist ${e.detail}`, 404)
        } else if (e.code === '23505') {
            throw new ServerError(`Unique error ${e.detail}`, 404)
        }
        throw e
    }
}

const getById = async (id) => {
    logger.info(`Getting basket with id ${id}`)

    const baskets = await query('SELECT * FROM basket WHERE id=$1', [id])

    if (baskets.length !== 1) {
        throw new ServerError('basket does not exist!', 404)
    }

    return baskets[0]
}

const update = async (id, { count, purchased }) => {
    logger.info(`Update basket with id ${id}`)

    if (count <= 0 || !Number.isInteger(count)) {
        throw new ServerError(`'count' must be a positive integer number.`, 400)
    }

    // validate the count for the product
    const baskets = await query('SELECT * FROM basket WHERE id=$1', [id])
    if (baskets.length !== 1) {
        throw new ServerError('Basket does not exist!', 404)
    }

    if (count === undefined) count = baskets[0].count

    const products = await query('SELECT * FROM product WHERE id=$1', [baskets[0].product_id])
    if (products.length !== 1) {
        throw new ServerError('Product does not exist!', 404)
    }

    if (products[0].total - products[0].sold < count) {
        throw new ServerError(`'count' exceeds the available value`, 400)
    }

    try {
        const baskets = purchased
            ? await query(`UPDATE basket SET count=$2, purchased=TRUE WHERE id=$1 RETURNING *`, [id, count])
            : await query(`UPDATE basket SET count=$2 WHERE id=$1 RETURNING *`, [id, count])

        if (baskets.length !== 1) {
            throw new ServerError('Basket does not exist', 404)
        }

        return baskets[0]
    } catch (e) {
        if (e.code === '23503') {
            throw new ServerError(`Foreign key element does not exist ${e.detail}`, 404)
        }
        throw e
    }
}

const remove = async (id) => {
    logger.info(`Deleting basket with id ${id}`)

    await query('DELETE FROM basket WHERE id=$1', [id])
}

module.exports = {
    getAll,
    getForUser,
    create,
    getById,
    update,
    remove
}
