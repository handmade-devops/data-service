const { Pool } = require('pg')
const config = require('./config')

const pool = new Pool(config.DB_OPTIONS)

const query = async (text, params) => {
    const { rows } = await pool.query(text, params)
    return rows
}

module.exports = {
    query,
}
