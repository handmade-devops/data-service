const formatQueryString = (params) => {
    let queryString = ''
    const queryArray = []
    for (let [key, value] of Object.entries(params)) {
        if (value !== undefined) {
            queryString = queryString.concat(`, ${key}=$${queryArray.length + 2}`)
            queryArray.push(value)
        }
    }
    queryString = queryString.slice(2)
    return [queryString, queryArray];
};

module.exports = {
    formatQueryString,
}
