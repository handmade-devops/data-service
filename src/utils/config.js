const { getSecret } = require('docker-secret')

const DEV = 'development'

const PORT = process.env.PORT || 8080
const NODE_ENV = process.env.NODE_ENV || 'development'
const DB_OPTIONS = {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    database: NODE_ENV === DEV ? process.env.DB_DATABASE : getSecret(process.env.DB_DATABASE_FILE),
    user: NODE_ENV === DEV ? process.env.DB_USERNAME : getSecret(process.env.DB_USERNAME_FILE),
    password: NODE_ENV === DEV ? process.env.DB_PASSWORD : getSecret(process.env.DB_PASSWORD_FILE),
}

module.exports = {
    PORT,
    NODE_ENV,
    DB_OPTIONS,
}
