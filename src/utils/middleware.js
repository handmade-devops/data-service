const logger = require('./logger')

const unknownEndpoint = (_req, res) => {
    res.status(404).send({ error: 'unknown endpoint' })
}

const errorHandler = (err, _req, res, _next) => {
    logger.error(err)

    let status = 500
    let message = 'Something Bad Happened'
    if (err.httpStatus) {
        status = err.httpStatus
        message = err.message
    }

    err.errors
        ? res.status(err.status).json({ message: err.message, errors: err.errors }) // openapi errors
        : res.status(status).json({ message }) // regular server errors
}

module.exports = {
    unknownEndpoint,
    errorHandler,
}
